#!/usr/bin/env bash
set -efuxo pipefail

nix-run cargo fmt
RUST_BACKTRACE=full nix-run cargo test
nix-run cargo clippy

find . -name '*.bash' -print0 | xargs --null shellcheck
