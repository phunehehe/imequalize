#! /usr/bin/env bash
set -efuxo pipefail
here=$(dirname "$0")
exec nix-run2 "$here/shell.nix" cargo run --release --manifest-path "$here/Cargo.toml" "$@"
