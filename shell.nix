let
  pkgs = import <nixpkgs> {};
  inherit (pkgs) lib stdenv;

in stdenv.mkDerivation rec {
  name = "env";

  buildInputs = [
    pkgs.cargo
    pkgs.rustfmt
    pkgs.clippy
  ];

  installPhase = ''
    mkdir $out
    ${lib.concatMapStringsSep "\n" (i: "ln -s ${i} $out/") buildInputs}
  '';

  phases = ["installPhase"];
}
