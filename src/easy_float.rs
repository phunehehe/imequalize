use std::cmp::Ordering;
use std::ops::{Deref, Div, Mul};

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct EasyFloat(pub f64);

impl Deref for EasyFloat {
    type Target = f64;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Div<EasyFloat> for EasyFloat {
    type Output = Self;
    fn div(self, other: EasyFloat) -> <Self as Div<EasyFloat>>::Output {
        EasyFloat(*self / *other)
    }
}

impl Mul<f64> for EasyFloat {
    type Output = Self;
    fn mul(self, other: f64) -> <Self as Mul<f64>>::Output {
        EasyFloat(*self * other)
    }
}

impl Eq for EasyFloat {}

impl From<usize> for EasyFloat {
    fn from(v: usize) -> Self {
        EasyFloat(v as f64)
    }
}

impl Ord for EasyFloat {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for EasyFloat {
    fn partial_cmp(&self, other: &EasyFloat) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
