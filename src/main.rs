extern crate image;

mod easy_float;

use crate::easy_float::EasyFloat;
use image::{DynamicImage, ImageBuffer};
use ndarray::{azip, s, stack, Array, Array2, Array3, ArrayView2, ArrayView3, Axis};
use std::collections::{BTreeMap, BTreeSet};
use std::env::{args, var};
use std::io::{stdin, stdout, Write};
use std::ops::Mul;
use std::path::Path;

fn read(path: &str) -> Array3<f64> {
    let image = image::open(path).unwrap();
    let nonlinear = image_to_nonlinear_rgb(image);
    let linear = nonlinear_rgb_to_linear(nonlinear);
    let lab = linear_rgb_to_lab(linear);
    lab_to_lch(lab)
}

fn write(l: ArrayView2<f64>, c: ArrayView2<f64>, h: ArrayView2<f64>, path: &str) {
    let lch = stack![Axis(2), l, c, h];
    let lab = lch_to_lab(lch);
    let linear = lab_to_linear_rgb(lab);
    let nonlinear = linear_rgb_to_nonlinear(linear);
    let image = nonlinear_rgb_to_image(nonlinear);
    image.save(path).unwrap()
}

fn image_to_array(image: DynamicImage) -> Array3<u16> {
    let buffer = image.into_rgb16();
    let width = buffer.width().try_into().unwrap();
    let height = buffer.height().try_into().unwrap();
    let vec = buffer.into_vec();
    Array::from_shape_vec((height, width, 3), vec).unwrap()
}

fn array_to_image(array: Array3<u16>) -> DynamicImage {
    let height = array.len_of(Axis(0)).try_into().unwrap();
    let width = array.len_of(Axis(1)).try_into().unwrap();
    let vec = array.into_flat().to_vec();
    let buffer = ImageBuffer::from_vec(width, height, vec).unwrap();
    DynamicImage::ImageRgb16(buffer)
}

fn image_to_nonlinear_rgb(image: DynamicImage) -> Array3<f64> {
    image_to_array(image).mapv(f64::from) / f64::from(u16::MAX)
}

fn nonlinear_rgb_to_image(nonlinear: Array3<f64>) -> DynamicImage {
    let nonlinear = nonlinear * f64::from(u16::MAX);
    let nonlinear = nonlinear.round().mapv(|x| x as u16);
    array_to_image(nonlinear)
}

// https://bottosson.github.io/posts/colorwrong/#what-can-we-do%3F
fn nonlinear_rgb_to_linear(nonlinear: Array3<f64>) -> Array3<f64> {
    nonlinear.mapv_into(|x| {
        if x >= 0.04045 {
            ((x + 0.055) / 1.055).powf(2.4)
        } else {
            x / 12.92
        }
    })
}

fn linear_rgb_to_nonlinear(linear: Array3<f64>) -> Array3<f64> {
    linear.mapv_into(|x| {
        if x >= 0.0031308 {
            1.055 * x.powf(1.0 / 2.4) - 0.055
        } else {
            12.92 * x
        }
    })
}

// https://bottosson.github.io/posts/oklab/#converting-from-linear-srgb-to-oklab
fn linear_rgb_to_lab(linear: Array3<f64>) -> Array3<f64> {
    let r = linear.index_axis(Axis(2), 0);
    let g = linear.index_axis(Axis(2), 1);
    let b = linear.index_axis(Axis(2), 2);

    let l = r.mul(0.4122214708) + g.mul(0.5363325363) + b.mul(0.0514459929);
    let m = r.mul(0.2119034982) + g.mul(0.6806995451) + b.mul(0.1073969566);
    let s = r.mul(0.0883024619) + g.mul(0.2817188376) + b.mul(0.6299787005);

    let l = l.cbrt();
    let m = m.cbrt();
    let s = s.cbrt();

    let l2 = l.view().mul(0.2104542553) + m.view().mul(0.7936177850) - s.view().mul(0.0040720468);
    let a2 = l.view().mul(1.9779984951) - m.view().mul(2.4285922050) + s.view().mul(0.4505937099);
    let b2 = l.mul(0.0259040371) + m.mul(0.7827717662) - s.mul(0.8086757660);
    stack![Axis(2), l2, a2, b2]
}

fn lab_to_linear_rgb(lab: Array3<f64>) -> Array3<f64> {
    let l = lab.index_axis(Axis(2), 0);
    let a = lab.index_axis(Axis(2), 1);
    let b = lab.index_axis(Axis(2), 2);

    let l2 = l.mul(1.0) + a.mul(0.3963377774) + b.mul(0.2158037573);
    let m2 = l.mul(1.0) - a.mul(0.1055613458) - b.mul(0.0638541728);
    let s2 = l.mul(1.0) - a.mul(0.0894841775) - b.mul(1.2914855480);

    let l2 = l2.powi(3);
    let m2 = m2.powi(3);
    let s2 = s2.powi(3);

    let r = l2.view().mul(4.0767416621) - m2.view().mul(3.3077115913) + s2.view().mul(0.2309699292);
    let g =
        l2.view().mul(-1.2684380046) + m2.view().mul(2.6097574011) - s2.view().mul(0.3413193965);
    let b = l2.mul(-0.0041960863) - m2.mul(0.7034186147) + s2.mul(1.7076147010);
    stack![Axis(2), r, g, b]
}

// https://bottosson.github.io/posts/oklab/#the-oklab-color-space
fn lab_to_lch(lab: Array3<f64>) -> Array3<f64> {
    let l = lab.index_axis(Axis(2), 0);
    let a = lab.index_axis(Axis(2), 1);
    let b = lab.index_axis(Axis(2), 2);
    let c = (a.pow2() + b.pow2()).sqrt();
    let mut h = Array2::zeros(l.dim());
    azip!((h in &mut h, &a in &a, &b in &b) *h = b.atan2(a));
    stack![Axis(2), l, c, h]
}

fn lch_to_lab(lch: Array3<f64>) -> Array3<f64> {
    let l = lch.index_axis(Axis(2), 0);
    let c = lch.index_axis(Axis(2), 1);
    let h = lch.index_axis(Axis(2), 2);

    let mut a = Array2::zeros(l.dim());
    azip!((a in &mut a, &c in &c, &h in &h) *a = c * h.cos());

    let mut b = Array2::zeros(l.dim());
    azip!((b in &mut b, &c in &c, &h in &h) *b = c * h.sin());

    stack![Axis(2), l, a, b]
}

fn equalize(input: ArrayView2<f64>, max_value: f64) -> Array2<f64> {
    let values = BTreeSet::from_iter(input.flatten().map(|x| EasyFloat(*x)));
    let max_index = EasyFloat::from(values.len() - 1);
    let map = BTreeMap::from_iter(
        values
            .iter()
            .enumerate()
            .map(|(i, v)| (v, EasyFloat::from(i) * max_value / max_index)),
    );
    input.mapv(|x| *map[&EasyFloat(x)])
}

fn median(array: ArrayView2<f64>) -> f64 {
    let mut vec = array.flatten().to_vec();
    vec.sort_by(|a, b| a.total_cmp(b));
    let len = vec.len();
    vec[len / 2]
}

fn interpolate(array: ArrayView2<f64>, reference: f64, target: f64, max: f64) -> Array2<f64> {
    array.mapv(|x| {
        if x < reference {
            x / reference * target
        } else {
            max - ((max - x) / (max - reference) * (max - target))
        }
    })
}

fn center_stats<E>(lch: ArrayView3<f64>, var: Result<String, E>, input_path: &str) -> (f64, f64) {
    let (left, right, top, bottom) = match var {
        Ok(input) => {
            let mut input = input.split(|c| !char::is_ascii_digit(&c));
            let width: usize = input.next().unwrap().parse().unwrap();
            let height: usize = input.next().unwrap().parse().unwrap();
            let left: usize = input.next().unwrap().parse().unwrap();
            let top: usize = input.next().unwrap().parse().unwrap();
            assert!(input.next().is_none());
            let right = left + width;
            let bottom = top + height;
            println!("{:?}", (width, height, left, top));
            println!("{:?}", (left, right, top, bottom));
            (left, right, top, bottom)
        }
        _ => {
            let height = lch.len_of(Axis(0));
            let width = lch.len_of(Axis(1));
            let top = height * 4 / 9;
            let bottom = height * 5 / 9;
            let left = width * 4 / 9;
            let right = width * 5 / 9;
            println!("{:?}", (left, right, top, bottom));
            (left, right, top, bottom)
        }
    };

    let l = lch.slice(s![top..bottom, left..right, 0]);
    let c = lch.slice(s![top..bottom, left..right, 1]);
    let h = lch.slice(s![top..bottom, left..right, 2]);
    let output_path = format!("{}-center.png", input_path);
    write(l, c, h, &output_path);

    let center_l = median(l);
    let center_c = median(c);
    (center_l, center_c)
}

fn find_l1<E>(
    var: Result<String, E>,
    l: ArrayView2<f64>,
    c: ArrayView2<f64>,
    h: ArrayView2<f64>,
    center_l: f64,
    input_path: &str,
) -> (f64, Array2<f64>) {
    let input = match var {
        Ok(input) => input,

        _ => {
            for i in 1..20 {
                let l1 = 0.05 * f64::from(i);
                let l = interpolate(l, center_l, l1, 1.0);
                let output_path = format!("{}-{:.0}.png", input_path, l1 * 100.0);
                write(l.view(), c, h, &output_path);
            }

            print!("L1: ");
            stdout().flush().unwrap();
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            input
        }
    };
    let mut l1: f64 = input.trim_end().parse().unwrap();
    l1 /= 100.0;
    (l1, interpolate(l, center_l, l1, 1.0))
}

fn find_c1<E>(
    var: Result<String, E>,
    l: ArrayView2<f64>,
    c: ArrayView2<f64>,
    h: ArrayView2<f64>,
    l1: f64,
    center_c: f64,
    input_path: &str,
) -> (f64, Array2<f64>) {
    let input = match var {
        Ok(input) => input,

        _ => {
            for i in 1..20 {
                let c1 = 0.01 * f64::from(i);
                let c = interpolate(c, center_c, c1, 0.4);
                let output_path = format!("{}-{:.0}-{:.0}.png", input_path, l1 * 100.0, c1 * 100.0);
                write(l, c.view(), h, &output_path);
            }

            print!("C1: ");
            stdout().flush().unwrap();
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            input
        }
    };
    let mut c1: f64 = input.trim_end().parse().unwrap();
    c1 /= 100.0;
    (c1, interpolate(c, center_c, c1, 0.4))
}

fn find_l2<E>(
    var: Result<String, E>,
    l: ArrayView2<f64>,
    c: ArrayView2<f64>,
    h: ArrayView2<f64>,
    l1: f64,
    c1: f64,
    input_path: &str,
) -> (f64, Array2<f64>) {
    let l_equalized = equalize(l, 1.0);
    let input = match var {
        Ok(input) => input,

        _ => {
            for i in 1..=20 {
                let l2 = 0.05 * f64::from(i);
                let l = l_equalized.view().mul(l2) + l.mul(1.0 - l2);
                let output_path = format!(
                    "{}-{:.0}-{:.0}-{:.0}.png",
                    input_path,
                    l1 * 100.0,
                    c1 * 100.0,
                    l2 * 100.0
                );
                write(l.view(), c, h, &output_path);
            }

            print!("L2: ");
            stdout().flush().unwrap();
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            input
        }
    };
    let mut l2: f64 = input.trim_end().parse().unwrap();
    l2 /= 100.0;
    (l2, l_equalized.view().mul(l2) + l.mul(1.0 - l2))
}

fn find_c2<E>(
    var: Result<String, E>,
    l: ArrayView2<f64>,
    c: ArrayView2<f64>,
    h: ArrayView2<f64>,
    l1: f64,
    c1: f64,
    l2: f64,
    input_path: &str,
) -> (f64, Array2<f64>) {
    let c_equalized = equalize(c, 0.4);
    let input = match var {
        Ok(input) => input,

        _ => {
            for i in 1..=20 {
                let c2 = 0.05 * f64::from(i);
                let c = c_equalized.view().mul(c2) + c.mul(1.0 - c2);
                let output_path = format!(
                    "{}-{:.0}-{:.0}-{:.0}-{:.0}.png",
                    input_path,
                    l1 * 100.0,
                    c1 * 100.0,
                    l2 * 100.0,
                    c2 * 100.0
                );
                write(l, c.view(), h, &output_path);
            }

            print!("C2: ");
            stdout().flush().unwrap();
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            input
        }
    };
    let mut c2: f64 = input.trim_end().parse().unwrap();
    c2 /= 100.0;
    (c2, c_equalized.view().mul(c2) + c.mul(1.0 - c2))
}

fn main() {
    let input_path = args().nth(1).unwrap();
    let input_path = Path::new(&input_path).canonicalize().unwrap();
    let input_path = input_path.to_str().unwrap();
    let lch = read(input_path);

    let (center_l, center_c) = center_stats(lch.view(), var("CENTER"), input_path);
    println!("{:?}\t{:?}", center_l, center_c);

    let l = lch.index_axis(Axis(2), 0);
    let c = lch.index_axis(Axis(2), 1);
    let h = lch.index_axis(Axis(2), 2);

    let (l1, l) = find_l1(var("L1"), l, c, h, center_l, input_path);
    let (c1, c) = find_c1(var("C1"), l.view(), c, h, l1, center_c, input_path);

    let (l2, l) = find_l2(var("L2"), l.view(), c.view(), h, l1, c1, input_path);
    let (c2, c) = find_c2(var("C2"), l.view(), c.view(), h, l1, c1, l2, input_path);

    println!("{:?}\t{:?}\t{:?}\t{:?}", l1, c1, l2, c2);
    let output_path = format!(
        "{}-{:.0}-{:.0}-{:.0}-{:.0}.png",
        input_path,
        l1 * 100.0,
        c1 * 100.0,
        l2 * 100.0,
        c2 * 100.0
    );
    write(l.view(), c.view(), h, &output_path);
}

#[cfg(test)]
mod tests {
    use super::*;
    use approx::assert_abs_diff_eq;
    use ndarray::array;
    use std::f64::consts::PI;

    #[test]
    fn test_image_to_nonlinear_rgb() {
        let image_array = array![
            [[0, 0, 0], [0, 0, 65535], [0, 65535, 0], [65535, 0, 0]],
            [
                [65535, 0, 32768],
                [0, 32768, 65535],
                [32768, 65535, 0],
                [65535, 0, 32768]
            ]
        ];
        let nonlinear = array![
            [
                [0.0, 0.0, 0.0],
                [0.0, 0.0, 1.0],
                [0.0, 1.0, 0.0],
                [1.0, 0.0, 0.0],
            ],
            [
                [1.0, 0.0, 0.5],
                [0.0, 0.5, 1.0],
                [0.5, 1.0, 0.0],
                [1.0, 0.0, 0.5]
            ]
        ];
        let image = array_to_image(image_array.clone());
        assert_abs_diff_eq!(image_to_nonlinear_rgb(image), nonlinear, epsilon = 1e-5);

        let image = nonlinear_rgb_to_image(nonlinear);
        assert_eq!(image_to_array(image), image_array);
    }

    #[test]
    fn test_nonlinear_rgb_to_linear() {
        let nonlinear_input = array![[
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
            [0.2, 0.3, 0.6], // https://apps.colorjs.io/convert/?color=rgb(20%25%2030%25%2060%25)
        ]];

        let expected = array![[
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
            [0.033104, 0.073238, 0.318546],
        ]];
        let linear = nonlinear_rgb_to_linear(nonlinear_input.clone());
        assert_abs_diff_eq!(linear, expected, epsilon = 1e-6);

        let lab = linear_rgb_to_lab(linear);
        let lch = lab_to_lch(lab);
        let lab = lch_to_lab(lch);
        let linear = lab_to_linear_rgb(lab);
        let nonlinear_output = linear_rgb_to_nonlinear(linear);
        assert_abs_diff_eq!(nonlinear_output, nonlinear_input, epsilon = 1e-5);
    }

    #[test]
    fn test_linear_rgb_to_nonlinear() {
        let input = array![[
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
            [0.2, 0.3, 0.6], // https://apps.colorjs.io/convert/?color=color(srgb-linear%200.2%200.3%200.6)
        ]];

        let expected = array![[
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
            [0.484529, 0.583831, 0.797737]
        ]];
        assert_abs_diff_eq!(linear_rgb_to_nonlinear(input), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_linear_rgb_to_lab() {
        let input = array![[
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
            [0.2, 0.3, 0.6], // https://apps.colorjs.io/convert/?color=color(srgb-linear%200.2%200.3%200.6)
        ]];

        let expected = array![[
            [0.0, 0.0, 0.0],
            [0.452013, -0.032456, -0.311528],
            [0.866439, -0.233887, 0.179498],
            [0.905399, -0.149443, -0.039398],
            [0.627955, 0.224863, 0.125846],
            [0.701673, 0.274566, -0.169156],
            [0.967982, -0.071369, 0.198569],
            [1.0, 0.0, 0.0],
            [0.671243, -0.007566, -0.086056],
        ]];
        assert_abs_diff_eq!(linear_rgb_to_lab(input), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_lab_to_linear_rgb() {
        let input = array![[
            [0.0, -1.0, -1.0],
            [0.0, -1.0, 1.0],
            [0.0, 1.0, -1.0],
            [0.0, 1.0, 1.0],
            [1.0, -1.0, -1.0],
            [1.0, -1.0, 1.0],
            [1.0, 1.0, -1.0],
            [1.0, 1.0, 1.0],
            [0.4, -0.1, 0.2], // https://apps.colorjs.io/convert/?color=oklab(0.4%20-0.1%200.2)
        ]];

        let expected = array![[
            [-0.342920, -0.595260, 4.494743],
            [-0.425344, 0.600408, -2.965573],
            [0.425344, -0.600408, 2.965573],
            [0.342920, 0.595260, -4.494743],
            [-1.934295, -0.507495, 21.923807],
            [-1.497586, 2.254905, -0.811537],
            [6.262534, -3.434561, 17.606420],
            [15.173293, -3.800447, -0.515056],
            [0.060467, 0.079752, -0.038712]
        ]];
        assert_abs_diff_eq!(lab_to_linear_rgb(input), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_lab_to_lch() {
        let input = array![[
            [0.0, -1.0, -1.0],
            [0.0, -1.0, 1.0],
            [0.0, 1.0, -1.0],
            [0.0, 1.0, 1.0],
            [1.0, -1.0, -1.0],
            [1.0, -1.0, 1.0],
            [1.0, 1.0, -1.0],
            [1.0, 1.0, 1.0],
            [0.4, -0.1, 0.2], // https://apps.colorjs.io/convert/?color=oklab(0.4%20-0.1%200.2)
        ]];

        let expected = array![[
            [0.0, 1.414213, -2.356194],
            [0.0, 1.414213, 2.356194],
            [0.0, 1.414213, -0.785398],
            [0.0, 1.414213, 0.785398],
            [1.0, 1.414213, -2.356194],
            [1.0, 1.414213, 2.356194],
            [1.0, 1.414213, -0.785398],
            [1.0, 1.414213, 0.785398],
            [0.4, 0.223606, 2.034443]
        ]];
        assert_abs_diff_eq!(lab_to_lch(input), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_lch_to_lab() {
        let input = array![[
            [0.0, 0.0, -PI],
            [0.0, 0.0, -PI / 2.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, PI / 2.0],
            [0.0, 0.0, PI],
            [0.0, 1.0, -PI],
            [0.0, 1.0, -PI / 2.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, PI / 2.0],
            [0.0, 1.0, PI],
            [1.0, 0.0, -PI],
            [1.0, 0.0, -PI / 2.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, PI / 2.0],
            [1.0, 0.0, PI],
            [1.0, 1.0, -PI],
            [1.0, 1.0, -PI / 2.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, PI / 2.0],
            [1.0, 1.0, PI],
            [0.6, 0.1, PI / 9.0], // https://apps.colorjs.io/convert/?color=oklch(0.6%200.1%2020)
        ]];

        let expected = array![[
            [0.0, -0.0, -0.0],
            [0.0, 0.0, -0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, -0.0, 0.0],
            [0.0, -1.0, 0.0],
            [0.0, 0.0, -1.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, -1.0, 0.0],
            [1.0, -0.0, -0.0],
            [1.0, 0.0, -0.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [1.0, -0.0, 0.0],
            [1.0, -1.0, 0.0],
            [1.0, 0.0, -1.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, -1.0, 0.0],
            [0.6, 0.093969, 0.034202]
        ]];
        assert_abs_diff_eq!(lch_to_lab(input), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_equalize() {
        let input = array![[0.0, 0.1, 0.9, 1.0]];
        let expected = array![[0.0, 0.3, 0.6, 0.9]];
        assert_abs_diff_eq!(equalize(input.view(), 0.9), expected, epsilon = 1e-6);
    }

    #[test]
    fn test_interpolate() {
        let input = array![[0.0, 0.2, 0.3, 0.7, 0.9]];
        let expected = array![[0.0, 0.4, 0.6, 0.8, 0.9]];
        assert_abs_diff_eq!(
            interpolate(input.view(), 0.3, 0.6, 0.9),
            expected,
            epsilon = 1e-6
        );
    }
}
